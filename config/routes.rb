Rails.application.routes.draw do
    get 'products/index'
    root 'products#index'
    get 'products/new', to: 'products#new'
    post 'products', to: 'products#create'
end
